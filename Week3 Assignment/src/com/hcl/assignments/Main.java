package com.hcl.assignments;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		MagicOfBooks store = new MagicOfBooks();

		do{
			System.out.println("[========== Magic Of Books ==========]\n"+"1. Add a new book\n" + "2. Delete a book\r\n" + "3. Update a book\n"
					+ "4. Display all the books\n" + "5. Total count of the books\n"
					+ "6. Search for autobiography genre books\n" + "7. Display by different features");
			System.out.println("-----------------------------------------");
			System.out.println("Enter your choice:");
			int choice = scan.nextInt();

			switch (choice) {

			case 1:// Adding a book
				System.out.println("Enter no. of books you want to add");
				int n = scan.nextInt();

				for (int i = 1; i <= n; i++) {
					store.addbook();
				}
				break;

			case 2:// Deleting a book
				try {
					store.deletebook();
				} catch (CustomException e) {
					// TODO Auto-generated catch block
					System.err.println(e.getMessage());
				}
				break;

			case 3:// Updating a book
				try {
					store.updatebook();
				} catch (CustomException e) {
					// TODO Auto-generated catch block
					System.err.println(e.getMessage());
				}
				break;

			case 4:// Displaying books
				try {
					store.displayBook();
				} catch (CustomException e) {
					// TODO Auto-generated catch block
					System.err.println(e.getMessage());
				}
				break;

			case 5:// Total no. of books
				try {
					store.bookCount();
				} catch (CustomException e) {
					// TODO Auto-generated catch block
					System.err.println(e.getMessage());
				}
				break;

			case 6:// Searching for particular book
				try {
					store.autobiographyBooks();
				} catch (CustomException e) {
					// TODO Auto-generated catch block
					System.err.println(e.getMessage());
				}
				break;

			case 7:// Displaying books by different features
				System.out.println(
						"Enter your choice:\n1. Price low to high"+"\n2. Price high to low"+"\n3. Best selling");
				int ch = scan.nextInt();

				switch (ch) {

				case 1://displaying books with price low to high
					try {
						store.displayByParicularFeature(1);
					} catch (CustomException e) {
						// TODO Auto-generated catch block
						System.err.println(e.getMessage());
					}
					break;
				case 2://displaying books with price high to low
					try {
						store.displayByParicularFeature(2);
					} catch (CustomException e) {
						// TODO Auto-generated catch block
						System.err.println(e.getMessage());
					}
					break;
				case 3://displaying best selling book
					try {
						store.displayByParicularFeature(3);
					} catch (CustomException e) {
						// TODO Auto-generated catch block
						System.err.println(e.getMessage());
					}
					break;
				}

			default:
				System.out.println("Wrong choice...Check again");
			}

		}while(true);

	}

}
