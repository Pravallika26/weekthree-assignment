package com.hcl.assignments;

public class CustomException extends Exception{
	
	public CustomException(String s) {
		super(s);
	}

}
