package com.hcl.assignments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class MagicOfBooks<T> {

	Scanner scan = new Scanner(System.in);

	Map<Integer, Book> bookMap = new HashMap<>();
	Map<Double, Book> priceMap = new TreeMap<>();

	List<Book> booklist = new ArrayList<Book>();

	public void addbook() {   //adding books 

		Book book = new Book();

		System.out.println("Enter book id");
		book.setBookId(scan.nextInt());

		System.out.println("Enter book name");
		book.setName(scan.next());

		System.out.println("Enter book price");
		book.setPrice(scan.nextDouble());

		System.out.println("Enter book genre");
		book.setGenre(scan.next());

		System.out.println("Enter number of copies sold");
		book.setCopiesSold(scan.nextInt());

		System.out.println("Enter book status");
		book.setBookStatus(scan.next());

		bookMap.put(book.getBookId(), book);
		System.out.println("New Books added");

		priceMap.put(book.getPrice(), book);
		booklist.add(book);

		System.out.println(bookMap);

	}

	public void deletebook() throws CustomException {   

		if (bookMap.isEmpty()) {
			throw new CustomException("Books are not available to delete...Check again");
		} else {            //deleting book using book id
			System.out.println("Enter book id that you want to delete");
			int id = scan.nextInt();
			bookMap.remove(id);
			System.out.println("Book deleted.!");
			System.out.println(bookMap);
		}

	}

	public void updatebook() throws CustomException {

		if (bookMap.isEmpty()) {
			throw new CustomException("Books are not available to update...Check again");
		} else {            //updating book
			Book update = new Book();

			System.out.println("Enter book id");
			update.setBookId(scan.nextInt());

			System.out.println("Enter book name");
			update.setName(scan.next());

			System.out.println("Enter book price");
			update.setPrice(scan.nextDouble());

			System.out.println("Enter book genre");
			update.setGenre(scan.next());

			System.out.println("Enter number of copies sold");
			update.setCopiesSold(scan.nextInt());

			System.out.println("Enter book status");
			update.setBookStatus(scan.next());

			bookMap.replace(update.getBookId(), update);
			System.out.println("Book details Updated");
			System.out.println(bookMap);
		}

	}

	public void displayBook() throws CustomException {

		if (bookMap.size() > 0) {       //displaying all books 
			Set<Integer> keySet = bookMap.keySet();

			Iterator<Integer> it = keySet.iterator();
			while (it.hasNext()) {
				Integer key = it.next();
				System.out.println(bookMap.get(key));
			}
		} else {
			throw new CustomException("No books to display");
		}

	}

	public void bookCount() throws CustomException {

		if (bookMap.isEmpty()) {
			throw new CustomException("No books to count");
		} else {          //counting total no. of books
			System.out.print("Count of all books : ");
			System.out.println(bookMap.size());
		}
	}

	public void autobiographyBooks() throws CustomException {

		String particularGenre = "Autobiography";

		if (booklist.isEmpty()) {
			throw new CustomException("Book store is empty...Check again");
		} else {            //displaying autobiography books
			List<Book> genreList = new ArrayList<Book>();

			ListIterator<Book> it = (booklist.listIterator());

			while (it.hasNext()) {
				Book book = it.next();
				if (book.genre.equals(particularGenre)) {
					genreList.add(book);
					System.out.println(genreList);
				}
			}
		}
	}

	public void displayByParicularFeature(int flag) throws CustomException {           //displaying different features

		if (flag == 1) {      
			if (priceMap.size() > 0) {
				Set<Double> keySet = priceMap.keySet();
				for (Double key : keySet) {
					System.out.println(key + "=" + priceMap.get(key));
				}
			} else {
				throw new CustomException("Book store is empty...Check again");
			}
		}

		if (flag == 2) {
			Map<Double, Object> priceMapReverseOrder = new TreeMap<>(priceMap);
			NavigableMap<Double, Object> navmap = ((TreeMap<Double, Object>) priceMapReverseOrder).descendingMap();
			System.out.println("Details of Book:");

			if (navmap.size() > 0) {
				Set<Double> keySet = navmap.keySet();
				for (Double key : keySet) {
					System.out.println(key + " = " + navmap.get(key));
				}
			} else {
				throw new CustomException("Book store is empty...Check again");
			}
		}

		if (flag == 3) {
			String bestSelling = "BestBook";

			List<Book> genreList = new ArrayList<Book>();
			ListIterator<Book> it = booklist.listIterator();

			while (it.hasNext()) {
				Book book = it.next();
				if (book.bookStatus.equals(bestSelling)) {
					genreList.add(book);
				}
			}
			if (genreList.isEmpty()) {
				throw new CustomException("No such books available");
			} else {
				System.out.println("Best selling books are: "+genreList);
			}
		}
	}
}
