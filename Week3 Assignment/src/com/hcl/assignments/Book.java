package com.hcl.assignments;

public class Book {
	
	String name;
	int bookId;
	double price;
	String genre;
	int copiesSold;
	String bookStatus;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public int getCopiesSold() {
		return copiesSold;
	}
	public void setCopiesSold(int copiesSold) {
		this.copiesSold = copiesSold;
	}
	public String getBookStatus() {
		return bookStatus;
	}
	public void setBookStatus(String bookStatus) {
		this.bookStatus = bookStatus;
	}
	@Override
	public String toString() {
		return "Book [name=" + name + ", bookId=" + bookId + ", price=" + price + ", genre=" + genre + ", copiesSold="
				+ copiesSold + ", bookStatus=" + bookStatus + "]";
	}
	
}
